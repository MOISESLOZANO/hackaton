package com.techu.apirest.repository;

import com.techu.apirest.model.PersonaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends MongoRepository <PersonaModel,String>{
}
