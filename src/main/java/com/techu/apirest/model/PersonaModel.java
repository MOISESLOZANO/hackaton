package com.techu.apirest.model;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.annotation.Documented;

@Document(collection="personas")
public class PersonaModel {

    @Id
    @NotNull
    private String id;
    private String name;
    private String profile;
    private String photoImagen;
    private String photoAlt;
    private Integer yearsInCompany;

    public PersonaModel(@NotNull String id, String name, String profile, String photoImagen, String photoAlt, Integer yearsInCompany) {
        this.id = id;
        this.name = name;
        this.profile = profile;
        this.photoImagen = photoImagen;
        this.photoAlt = photoAlt;
        this.yearsInCompany = yearsInCompany;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPhotoImagen() {
        return photoImagen;
    }

    public void setPhotoImagen(String photoImagen) {
        this.photoImagen = photoImagen;
    }

    public String getPhotoAlt() {
        return photoAlt;
    }

    public void setPhotoAlt(String photoAlt) {
        this.photoAlt = photoAlt;
    }

    public Integer getYearsInCompany() {
        return yearsInCompany;
    }

    public void setYearsInCompany(Integer yearsInCompany) {
        this.yearsInCompany = yearsInCompany;
    }
}
