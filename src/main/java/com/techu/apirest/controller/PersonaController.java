package com.techu.apirest.controller;

import com.techu.apirest.ProductoPrecio;
import com.techu.apirest.model.PersonaModel;
import com.techu.apirest.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiHackaton/v1")
public class PersonaController {

    @Autowired
    PersonaService personaService;
    ProductoPrecio productoPrecio;

    //Trae todas las personas de la colección
    @GetMapping("/personas")
    public List<PersonaModel> getPersonas(){

        return personaService.findAll();
    }

    //Trae un persona por ID
    @GetMapping("/personas/{id}")
    public Optional<PersonaModel> getPersonaId(@PathVariable String id){

        return personaService.findById(id);
    }


    @PostMapping("/personas")
    public PersonaModel postPersonas(@RequestBody PersonaModel newPersona){
        personaService.save(newPersona);
        return newPersona;
    }
    //Put tb se puede hacersin id tal como viene en el codelab y con void
    //Save extiende de mongodb y permite no solo añadir sino tb actualizar cuando el id existe. De ahi que el codigo sea igual que el post
    @PutMapping("/personas/{id}")
    public PersonaModel putPersonas(@RequestBody PersonaModel personaToUpdate, @PathVariable String id){
        if (personaService.findById(id).isPresent()){
        personaService.save(personaToUpdate);
        return personaToUpdate;
        }
        return new PersonaModel("null", "null", "null", "null", "null", 0);
    }

    //put sin id
    @PutMapping("/personas")
    public void putPersonas(@RequestBody PersonaModel personaToUpdate){
        personaService.save(personaToUpdate);
    }

    //boolean porque en service tb
    @DeleteMapping("/personas")
    public boolean deletePersonas(@RequestBody PersonaModel personaToDelete){
       return personaService.delete(personaToDelete);
    }

    @DeleteMapping("/personas/{id}")
    public boolean deletePersonas(@RequestBody PersonaModel personaToDelete, @PathVariable String id){

        if (personaService.findById(id).isPresent()){
            personaService.delete(personaToDelete);
            //se puede poner el return directamente arriba
            return true;
        }
        return false;
    }

    /*
    //Otra opcion mas sencilla de patch
   @PatchMapping("/productos/{id}")
    public PersonaModel patchProductos(@RequestBody PersonaModel productoToPatch, @PathVariable String id){

       productoPrecio.setPrecio(productoToPatch.getPrecio());

       personaService.save(productoToPatch);
       return productoToPatch;

    }

    @PatchMapping("/productos/{precio}")
    public boolean patchProductos(@RequestBody PersonaModel productoToPatch, @PathVariable Double precio){
        if (personaService.findById(productoToPatch.getId()).isPresent()){
            productoToPatch.setPrecio(precio);
            personaService.save(productoToPatch);
            return true;
        }else
            return false;

    }

    /*
    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody Map<String, Object> update, @PathVariable String id){
        ProductoModel productopatchear;
        Optional<ProductoModel> oproducto=productoService.findById(id);
        if (oproducto.isPresent()){
            productopatchear = oproducto.get();
        }
        else
            return new ProductoModel("0", "id no existe", 0.0);
        if (update.containsKey("precio"))
            productopatchear.setPrecio((Double) update.get("precio"));
        if (update.containsKey("descripcion"))
            productopatchear.setDescripcion((String) update.get("descripcion"));

       productoService.save(productopatchear);
       return productopatchear;

    }*/

}
