package com.techu.apirest.service;

import com.techu.apirest.model.PersonaModel;
import com.techu.apirest.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaService {

@Autowired
PersonaRepository personaRepository;

    //READ
    public List<PersonaModel> findAll(){

        return personaRepository.findAll();
    }

    //READ BY ID
    public Optional<PersonaModel> findById(String id){
        return personaRepository.findById(id);

    }

    //CREATE
    public PersonaModel save(PersonaModel producto){
        return personaRepository.save(producto);
    }


    //DELETE
    public boolean delete(PersonaModel producto){
        //otra opcion diferente a delete de holamundo
        //se coloca el try poque marca en rojo al no devolver nada
        try{
            personaRepository.delete(producto);
            return true;
        } catch (Exception e){
            return false;
        }

    }

}
